package attendance.stp;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import android.util.Log;

public class Labels {

	private String mPath;

	class Label {
		int number;
		String name;
		public Label(String name, int number) {
			this.name =name;
			this.number =number;
		}
	}

	List<Label> labels =new ArrayList<>();

	public Labels(String Path) {
		mPath = Path;
	}

	public void addLabelToList(String name, int number)	{
		labels.add(new Label(name,number));
	}

	public String getLabelName(int number) {
		Iterator<Label> iterator = labels.iterator();
		while (iterator.hasNext()) {
			Label label = iterator.next();
			if (label.number == number)
				return label.name;
		}
		return "";
	}

	public int getLabelNumber(String name) {
		Iterator<Label> iterator = labels.iterator();
		while (iterator.hasNext()) {
			Label label = iterator.next();
			if (label.name.equalsIgnoreCase(name))
				return label.number;
		}
		return -1;
	}

	public void saveLabelToFile() {
		try {
			File f=new File (mPath+"faces.txt");
			f.createNewFile();
			BufferedWriter bw = new BufferedWriter(new FileWriter(f));
			Iterator<Label> iterator = labels.iterator();
			while (iterator.hasNext()) {
				Label label = iterator.next();
				bw.write(label.name +","+label.number);
				bw.newLine();
			}
			bw.close();
		} catch (IOException e) {
			Log.e("error",e.getMessage()+" "+e.getCause());
			e.printStackTrace();
		}
	}

	public int maxLabelNumber() {
		int count = 0;
		Iterator<Label> iterator = labels.iterator();
		while (iterator.hasNext()) {
			Label label = iterator.next();
			if (label.number > count)
				count = label.number;
		}
		return count;
	}

}
