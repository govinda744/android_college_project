package attendance.stp;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.ToggleButton;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.JavaCameraView;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfRect;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.objdetect.CascadeClassifier;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class AddUserActivity extends AppCompatActivity implements CameraBridgeViewBase.CvCameraViewListener2 {

    private static final String TAG = "OCVSample::Activity";
    private static final Scalar FACE_RECT_COLOR = new Scalar(0, 255, 0, 255);
    public static final int JAVA_DETECTOR = 0;
    public static final int NATIVE_DETECTOR = 1;

    public static final int TRAINING= 0;
    public static final int IDLE= 2;

    static final long MAXIMG = 10;

    private int faceState = IDLE;

    private Mat mRgba;
    private Mat mGray;
    private CascadeClassifier cascadeClassifier;

    private int cameraIndex = CameraBridgeViewBase.CAMERA_ID_BACK;

    private int mAbsoluteFaceSize = 0;

    private String mPath="";

    private JavaCameraView mOpenCvCameraView;

    private String imageLabel;
    private ImageView imageView;
    private Bitmap mBitmap;
    private Handler setImageViewHandler;

    private PersonRecognizer personRecognizer;
    private ToggleButton capture;

    private int countImages = 0;

    static {
        OpenCVLoader.initDebug();
        System.loadLibrary("opencv_java");
    }

    public AddUserActivity() {
        String[] mDetectorName = new String[2];
        mDetectorName[JAVA_DETECTOR] = "Java";
        mDetectorName[NATIVE_DETECTOR] = "Native (tracking)";

        Log.i(TAG, "Instantiated new " + this.getClass());
    }

    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS:
                    Log.i(TAG, "OpenCV loaded successfully");

                    personRecognizer =new PersonRecognizer(mPath);
                    try {

                        cascadeClassifier = getCascadeClassifier();

                        if (cascadeClassifier.empty()) {
                            Log.e(TAG, "Failed to load cascade classifier");
                            cascadeClassifier = null;
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                        Log.e(TAG, "Failed to load cascade. Exception thrown: " + e);
                    }
                    mOpenCvCameraView.setCameraIndex(CameraBridgeViewBase.CAMERA_ID_BACK);
                    mOpenCvCameraView.enableView();
                break;
                default:
                    super.onManagerConnected(status);
                break;
            }
        }
    };

    private File createInternalAppDir(String dirName) {
        return getDir(dirName, Context.MODE_PRIVATE);
    }

    private File createInternalAppCascadeFile(File cascadeDir, String fileName) {
        return new File(cascadeDir, fileName);
    }

    private CascadeClassifier getCascadeClassifier() throws IOException {

        File cascadeDir = createInternalAppDir("cascade");
        File mCascadeFile = createInternalAppCascadeFile(cascadeDir, "LPBCascade.xml");

        InputStream frontalFaceCascadeInputStream = getResources().openRawResource(R.raw.lbpcascade_frontalface);
        FileOutputStream fileOutputStream = new FileOutputStream(mCascadeFile);

        byte[] buffer = new byte[4096];
        int bytesRead;
        while ((bytesRead = frontalFaceCascadeInputStream.read(buffer)) != -1) {
            fileOutputStream.write(buffer, 0, bytesRead);
        }
        frontalFaceCascadeInputStream.close();
        fileOutputStream.close();

        return new org.opencv.objdetect.CascadeClassifier(mCascadeFile.getAbsolutePath());
    }

    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_user);

        imageLabel = getIntent().getStringExtra("name");
        imageView = (ImageView) findViewById(R.id.imagePreview);

        capture = (ToggleButton) findViewById(R.id.capture);
        capture.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                triggerCameraAction();
            }
        });

        mOpenCvCameraView = (JavaCameraView) findViewById(R.id.java_surface_view);
        mOpenCvCameraView.setCvCameraViewListener(this);

        mOpenCvCameraView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mOpenCvCameraView.disableView();
                if(cameraIndex == CameraBridgeViewBase.CAMERA_ID_BACK) {
                    cameraIndex = CameraBridgeViewBase.CAMERA_ID_FRONT;
                    mOpenCvCameraView.setCameraIndex(cameraIndex);
                } else {
                    cameraIndex = CameraBridgeViewBase.CAMERA_ID_BACK;
                    mOpenCvCameraView.setCameraIndex(cameraIndex);
                }
                mOpenCvCameraView.enableView();
            }
        });

        mPath = Environment.getExternalStorageDirectory()+"/facerecogOCV/";

        Log.e("Path", mPath);

        setImageViewHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                if (msg.obj=="IMG")
                {
                    imageView.setImageBitmap(mBitmap);
                    if (countImages >= MAXIMG-1) {
                        capture.setChecked(false);
                        triggerCameraAction();
                    }
                }
            }
        };

        boolean success=(new File(mPath)).mkdirs();

        if (!success)
            Log.e("Error","Error creating directory");
    }

    void triggerCameraAction()
    {
        if (capture.isChecked())
            faceState = TRAINING;
        else {
            Toast.makeText(this, "Captured", Toast.LENGTH_SHORT).show();
            countImages=0;
            faceState=IDLE;
            imageView.setImageResource(R.drawable.user_image);
            Intent intent = new Intent(AddUserActivity.this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        }
    }

    @Override
    public void onCameraViewStarted(int width, int height) {
        mGray = new Mat();
        mRgba = new Mat();
    }

    @Override
    public void onCameraViewStopped() {
        mGray.release();
        mRgba.release();
    }

    @Override
    public Mat onCameraFrame(CameraBridgeViewBase.CvCameraViewFrame inputFrame) {

        mRgba = inputFrame.rgba();
        mGray = inputFrame.gray();

        if (mAbsoluteFaceSize == 0) {
            int height = mGray.rows();
            float mRelativeFaceSize = 0.2f;
            if (Math.round(height * mRelativeFaceSize) > 0) {
                mAbsoluteFaceSize = Math.round(height * mRelativeFaceSize);
            }
        }

        MatOfRect faces = new MatOfRect();

        if (JAVA_DETECTOR == JAVA_DETECTOR) {
            if (cascadeClassifier != null)
                cascadeClassifier.detectMultiScale(mGray, faces, 1.1, 3, 3,
                        new Size(mAbsoluteFaceSize, mAbsoluteFaceSize), new Size());
        } else {
            Log.e(TAG, "Detection method is not selected!");
        }

        Rect[] faceRectArray = faces.toArray();

        if ((faceRectArray.length == 1) && (faceState == TRAINING) && (countImages < MAXIMG) && (!imageLabel.equals(""))) {
            Mat imageMat;
            Rect rect = faceRectArray[0];

            imageMat = mRgba.submat(rect);
            mBitmap = Bitmap.createBitmap(imageMat.width(),imageMat.height(), Bitmap.Config.ARGB_8888);

            Utils.matToBitmap(imageMat, mBitmap);

            Message msg = new Message();
            msg.obj = "IMG";
            setImageViewHandler.sendMessage(msg);
            if (countImages < MAXIMG) {
                personRecognizer.addImageToPathDir(imageMat, imageLabel);
                countImages++;
            }
        }
        for (int i = 0; i < faceRectArray.length; i++)
            Core.rectangle(mRgba, faceRectArray[i].tl(), faceRectArray[i].br(), FACE_RECT_COLOR, 3);
        if (cameraIndex == CameraBridgeViewBase.CAMERA_ID_FRONT)
            Core.flip(mRgba, mRgba, 1);
        return mRgba;
    }

    @Override
    protected void onResume() {
        super.onResume();
        mLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mOpenCvCameraView.disableView();
    }
}
