package attendance.stp;

import static  com.googlecode.javacv.cpp.opencv_highgui.*;
import static  com.googlecode.javacv.cpp.opencv_core.*;

import static  com.googlecode.javacv.cpp.opencv_imgproc.*;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FilenameFilter;

import org.opencv.android.Utils;
import org.opencv.core.Mat;

import com.googlecode.javacv.cpp.opencv_imgproc;
import com.googlecode.javacv.cpp.opencv_contrib.FaceRecognizer;
import com.googlecode.javacv.cpp.opencv_core.IplImage;
import com.googlecode.javacv.cpp.opencv_core.MatVector;

import android.graphics.Bitmap;
import android.util.Log;

public  class PersonRecognizer {

	private FaceRecognizer faceRecognizer;
	private String mPath;
	private int count=0;
	private Labels labelsFile;

	private static  final int WIDTH= 128;
	private static  final int HEIGHT= 128;


	PersonRecognizer(String path) {
		faceRecognizer =  com.googlecode.javacv.cpp.opencv_contrib.createLBPHFaceRecognizer(1,8,8,8,123.0);
		mPath = path;
		labelsFile = new Labels(mPath);
	}

	void addImageToPathDir(Mat imageMat, String imageLabel) {

		Bitmap imageBitmap= Bitmap.createBitmap(imageMat.width(), imageMat.height(), Bitmap.Config.ARGB_8888);

		Utils.matToBitmap(imageMat,imageBitmap);
		imageBitmap=  Bitmap.createScaledBitmap(imageBitmap, WIDTH, HEIGHT, true);

		FileOutputStream fileOutputStream;
		try {
			fileOutputStream = new FileOutputStream(mPath+imageLabel+"-"+count+".png",true);
			count++;
			imageBitmap.compress(Bitmap.CompressFormat.PNG, 100, fileOutputStream);
			fileOutputStream.close();
		} catch (Exception e) {
			Log.e("error",e.getCause()+" "+e.getMessage());
			e.printStackTrace();

		}
	}

	public boolean train() {

		File root = new File(mPath);

		FilenameFilter pngFilter = new FilenameFilter() {
			public boolean accept(File dir, String name) {
				return name.toLowerCase().endsWith(".png");

			}
		};

		File[] imageFiles = root.listFiles(pngFilter);

		MatVector images = new MatVector(imageFiles.length);

		int[] labels = new int[imageFiles.length];

		int counter = 0;
		int labelNumber;

		IplImage normImg;
		IplImage grayImg;

		int mPathLength = mPath.length();

		for (File image : imageFiles) {
			String imageAbsolutePath = image.getAbsolutePath();
			normImg = cvLoadImage(imageAbsolutePath);

			if (normImg == null)
				Log.e("Error","Error cVLoadImage");
			Log.i("image",imageAbsolutePath);

			int lastIndexOfDash = imageAbsolutePath.lastIndexOf("-");

			String imageLabel = imageAbsolutePath.substring(mPathLength,lastIndexOfDash);

			if (labelsFile.getLabelNumber(imageLabel) < 0)
				labelsFile.addLabelToList(imageLabel, labelsFile.maxLabelNumber() + 1);

			labelNumber = labelsFile.getLabelNumber(imageLabel);

			grayImg = IplImage.create(normImg.width(), normImg.height(), IPL_DEPTH_8U, 1);

			cvCvtColor(normImg, grayImg, CV_BGR2GRAY);

			images.put(counter, grayImg);

			labels[counter] = labelNumber;

			counter++;
		}
		if (counter > 0)
			if (labelsFile.maxLabelNumber() > 1)
				faceRecognizer.train(images, labels);
		labelsFile.saveLabelToFile();
		return true;
	}

	public boolean canPredict()
	{
		if (labelsFile.maxLabelNumber() > 1)
			return true;
		else
			return false;

	}

	public String predict(Mat imageToPredict) {
		if (!canPredict())
			return "";
		int label[] = new int[1];
		double confidence[] = new double[1];
		IplImage iplImage = MatToIplImage(imageToPredict,WIDTH, HEIGHT);

		faceRecognizer.predict(iplImage, label, confidence);
		if (label[0] != -1 && confidence[0] < 60 ) {
			return  labelsFile.getLabelName(label[0]);
		}
		return "Unknown";
	}
	
	IplImage MatToIplImage(Mat imageMat,int width,int heigth) {

		Bitmap bmp=Bitmap.createBitmap(imageMat.width(), imageMat.height(), Bitmap.Config.ARGB_8888);

		Utils.matToBitmap(imageMat, bmp);
		return BitmapToIplImage(bmp,width, heigth);
	}

	IplImage BitmapToIplImage(Bitmap bmp, int width, int height) {

		if ((width != -1) || (height != -1)) {
			Bitmap bmp2 = Bitmap.createScaledBitmap(bmp, width, height, false);
			bmp = bmp2;
		}

		IplImage image = IplImage.create(bmp.getWidth(), bmp.getHeight(),
				IPL_DEPTH_8U, 4);

		bmp.copyPixelsToBuffer(image.getByteBuffer());

		IplImage grayImg = IplImage.create(image.width(), image.height(),
				IPL_DEPTH_8U, 1);

		cvCvtColor(image, grayImg, opencv_imgproc.CV_BGR2GRAY);

		return grayImg;
	}

	public void load() {
		train();
	}
}
