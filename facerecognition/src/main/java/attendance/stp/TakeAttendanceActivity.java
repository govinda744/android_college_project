package attendance.stp;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.JavaCameraView;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfRect;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.objdetect.CascadeClassifier;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashSet;
import java.util.Set;

public class TakeAttendanceActivity extends AppCompatActivity implements CameraBridgeViewBase.CvCameraViewListener2 {

    private static final String TAG = "OCVSample::Activity";
    private static final Scalar FACE_RECT_COLOR = new Scalar(0, 255, 0, 255);
    public static final int JAVA_DETECTOR = 0;

    public static final int SEARCHING = 1;
    public static final int IDLE = 2;

    private int faceState = IDLE;

    private int cameraIndex = CameraBridgeViewBase.CAMERA_ID_BACK;

    private Mat mRgba;
    private Mat mGray;
    private CascadeClassifier cascadeClassifier;

    private int mAbsoluteFaceSize = 0;

    private String mPath="";

    private JavaCameraView mOpenCvCameraView;

    private Handler mHandler;

    private PersonRecognizer personRecognizer;
    private ToggleButton scan;

    private Set<String> uniqueNames = new HashSet<>();

    private String[] uniqueNamesArray;

    static {
        OpenCVLoader.initDebug();
        System.loadLibrary("opencv_java");
    }

    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS:
                    Log.i(TAG, "OpenCV loaded successfully");

                    personRecognizer =new PersonRecognizer(mPath);
                    personRecognizer.load();

                    try {
                        cascadeClassifier = getCascadeClassifier();
                        if (cascadeClassifier.empty()) {
                            Log.e(TAG, "Failed to load cascade classifier");
                            cascadeClassifier = null;
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                        Log.e(TAG, "Failed to load cascade. Exception thrown: " + e);
                    }
                    mOpenCvCameraView.setCameraIndex(cameraIndex);
                    mOpenCvCameraView.enableView();
                break;
                default:
                    super.onManagerConnected(status);
                break;
            }
        }
    };



    private File createInternalAppDir(String dirName) {
        return getDir(dirName, Context.MODE_PRIVATE);
    }

    private File createInternalAppCascadeFile(File cascadeDir, String fileName) {
        return new File(cascadeDir, fileName);
    }

    private CascadeClassifier getCascadeClassifier() throws IOException {

        File cascadeDir = createInternalAppDir("cascade");
        File internalAppCascadeFile = createInternalAppCascadeFile(cascadeDir, "LPBCascade.xml");

        InputStream frontalFaceCascadeInputStream = getResources().openRawResource(R.raw.lbpcascade_frontalface);
        FileOutputStream outputStream = new FileOutputStream(internalAppCascadeFile);

        byte[] buffer = new byte[4096];
        int bytesRead;
        while ((bytesRead = frontalFaceCascadeInputStream.read(buffer)) != -1) {
            outputStream.write(buffer, 0, bytesRead);
        }
        frontalFaceCascadeInputStream.close();
        outputStream.close();

        return new CascadeClassifier(internalAppCascadeFile.getAbsolutePath());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_take_attendance);

        scan = (ToggleButton) findViewById(R.id.scan);
        final TextView results = (TextView) findViewById(R.id.results);

        mOpenCvCameraView = (JavaCameraView) findViewById(R.id.java_surface_view);
        mOpenCvCameraView.setCvCameraViewListener(this);

        mPath = Environment.getExternalStorageDirectory()+"/facerecogOCV/";

        Log.e("Path", mPath);

        mHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                String tempName = msg.obj.toString();
                if (!(tempName.equals("Unknown"))) {
                    tempName = capitalize(tempName);
                    uniqueNames.add(tempName);
                    uniqueNamesArray = uniqueNames.toArray(new String[uniqueNames.size()]);
                    StringBuilder strBuilder = new StringBuilder();
                    for (int i = 0; i < uniqueNamesArray.length; i++) {
                        strBuilder.append(uniqueNamesArray[i] + "\n");
                    }
                    String textToDisplay = strBuilder.toString();
                    results.setText(textToDisplay);
                }
            }
        };

        scan.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean toggle) {
                if(toggle) {
                    if(!personRecognizer.canPredict()) {
                        scan.setChecked(false);
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.SCanntoPredic), Toast.LENGTH_LONG).show();
                        return;
                    }
                    faceState = SEARCHING;
                }
                else {
                    faceState = IDLE;
                }
            }
        });

        mOpenCvCameraView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mOpenCvCameraView.disableView();
                if(cameraIndex == CameraBridgeViewBase.CAMERA_ID_BACK) {
                    cameraIndex = CameraBridgeViewBase.CAMERA_ID_FRONT;
                    mOpenCvCameraView.setCameraIndex(cameraIndex);
                } else {
                    cameraIndex = CameraBridgeViewBase.CAMERA_ID_BACK;
                    mOpenCvCameraView.setCameraIndex(cameraIndex);
                }
                mOpenCvCameraView.enableView();
            }
        });

        boolean success=(new File(mPath)).mkdirs();
        if (!success)
        {
            Log.e("Error","Error creating directory");
        }

        Button submit = (Button) findViewById(R.id.submit);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(uniqueNames.size() > 0) {
                    Intent intent = new Intent(TakeAttendanceActivity.this, ReviewResults.class);
                    intent.putExtra("list", uniqueNamesArray);
                    startActivity(intent);
                    finish();
                }
                else {
                    Toast.makeText(TakeAttendanceActivity.this, "Empty list cannot be sent further", Toast.LENGTH_LONG).show();
                }
            }
        });

    }

    @Override
    public void onCameraViewStarted(int width, int height) {
        mGray = new Mat();
        mRgba = new Mat();
    }

    @Override
    public void onCameraViewStopped() {
        mGray.release();
        mRgba.release();
    }

    @Override
    public Mat onCameraFrame(CameraBridgeViewBase.CvCameraViewFrame inputFrame) {
        mRgba = inputFrame.rgba();
        mGray = inputFrame.gray();

        if (mAbsoluteFaceSize == 0) {
            int height = mGray.rows();
            float mRelativeFaceSize = 0.2f;
            if (Math.round(height * mRelativeFaceSize) > 0) {
                mAbsoluteFaceSize = Math.round(height * mRelativeFaceSize);
            }
        }

        MatOfRect faces = new MatOfRect();

        int mDetectorType = JAVA_DETECTOR;
        if (mDetectorType == JAVA_DETECTOR) {
            if (cascadeClassifier != null)
                cascadeClassifier.detectMultiScale(mGray, faces, 1.1, 3, 3,
                        new Size(mAbsoluteFaceSize, mAbsoluteFaceSize), new Size());
        } else {
            Log.e(TAG, "Detection method is not selected!");
        }

        Rect[] facesArray = faces.toArray();

        if ((facesArray.length>0) && (faceState==SEARCHING))
        {
            Mat faceMat;
            faceMat = mGray.submat(facesArray[0]);
            Bitmap mBitmap = Bitmap.createBitmap(faceMat.width(), faceMat.height(), Bitmap.Config.ARGB_8888);


            Utils.matToBitmap(faceMat, mBitmap);
            Message msg = new Message();
            String textToChange = "IMG";
            msg.obj = textToChange;

            textToChange = personRecognizer.predict(faceMat);
            msg = new Message();
            msg.obj = textToChange;
            mHandler.sendMessage(msg);

        }
        for (int i = 0; i < facesArray.length; i++)
            Core.rectangle(mRgba, facesArray[i].tl(), facesArray[i].br(), FACE_RECT_COLOR, 3);
        if (cameraIndex == CameraBridgeViewBase.CAMERA_ID_FRONT)
            Core.flip(mRgba, mRgba, 1);
        return mRgba;
    }

    @Override
    protected void onResume() {
        super.onResume();
        mLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mOpenCvCameraView.disableView();
    }

    private String capitalize(final String line) {
        return Character.toUpperCase(line.charAt(0)) + line.substring(1);
    }
}
