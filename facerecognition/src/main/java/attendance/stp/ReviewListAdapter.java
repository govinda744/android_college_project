package attendance.stp;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;


import java.util.List;

public class ReviewListAdapter extends RecyclerView.Adapter<ReviewListAdapter.ReviewListViewHolder> {

    private List<String> data;
    private LayoutInflater inflater;
    private ReviewListAdapter.ClickListener clickListener;

    ReviewListAdapter(Context context, List<String> data) {
        inflater = LayoutInflater.from(context);
        this.data = data;
    }

    @Override
    public ReviewListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.review_list_row, parent, false);
        return new ReviewListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ReviewListViewHolder holder, int position) {

        holder.checkBox.setText(data.get(position));
        holder.checkBox.setChecked(true);

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    class ReviewListViewHolder extends RecyclerView.ViewHolder {

        CheckBox checkBox;

        ReviewListViewHolder(View itemView) {
            super(itemView);
            checkBox = (CheckBox) itemView.findViewById(R.id.checkBox);
            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    clickListener.onItemClick(data.get(getLayoutPosition()));
                }
            });
        }
    }

    interface ClickListener {
        void onItemClick(String name);
    }
}
